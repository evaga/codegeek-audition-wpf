﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="MainWindow.xaml.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the MainWindow.xaml type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.UI
{
    using System.Windows;

    using CodeGeekAuditionWPF.UI.ViewModels;

    /// <summary>
    /// The main window.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.Loaded += (sender, args) =>
            {
                var paymentViewModel = new PaymentViewModel();
                var paymentsListViewModel = new PaymentsListViewModel();

                this.PaymentView.DataContext = paymentViewModel;
                this.PaymentsTableView.DataContext = paymentsListViewModel;
            };
        }
    }
}
