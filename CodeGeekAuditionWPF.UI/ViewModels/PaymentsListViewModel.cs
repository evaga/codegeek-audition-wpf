﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="PaymentsListViewModel.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the PaymentsListViewModel type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.UI.ViewModels
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using CodeGeekAuditionWPF.DAL.Payments;
    using CodeGeekAuditionWPF.UI.Annotations;

    /// <summary>
    /// The payments list view model.
    /// </summary>
    public class PaymentsListViewModel: INotifyPropertyChanged
    {
        /// <summary>
        /// The property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the payments.
        /// </summary>
        public ObservableCollection<Payment> Payments
        {
            get
            {
                return PaymentsAggregator.Filtered;
            }
        }

        /// <summary>
        /// Gets or sets the selected payment.
        /// </summary>
        public Payment SelectedPayment
        {
            get
            {
                return PaymentsAggregator.Selected;
            }

            set
            {
                PaymentsAggregator.Selected = value;
            }
        }

        /// <summary>
        /// Gets the mouse down command.
        /// </summary>
        public RelayCommand MouseDownCommand
        {
            get
            {
                return new RelayCommand(this.MouseDown);
            }
        }

        /// <summary>
        /// Handles the <code>MouseDown</code> command.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        private void MouseDown(object obj)
        {
            this.SelectedPayment = null;
        }

        /// <summary>
        /// The <code>OnPropertyChanged</code> event handler.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
