﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="PaymentViewModel.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the PaymentViewModel type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.UI.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Windows;

    using CodeGeekAuditionWPF.DAL.Clients;
    using CodeGeekAuditionWPF.DAL.Payments;
    using CodeGeekAuditionWPF.UI.Annotations;

    /// <summary>
    /// The payment view model.
    /// </summary>
    public class PaymentViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// The property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the payment clients.
        /// </summary>
        public ObservableCollection<Client> PaymentClients
        {
            get
            {
                if (Clients == null)
                {
                    Clients = new ObservableCollection<Client>(new[] { AddClientStub }.Union(new ClientsRepository().Get()));
                }

                return Clients;
            }
        }

        /// <summary>
        /// The selected client.
        /// </summary>
        private Client selectedClient;

        /// <summary>
        /// Gets or sets the selected client.
        /// </summary>
        public Client SelectedClient
        {
            get
            {
                return this.selectedClient;
            }

            set
            {
                this.selectedClient = value;
                this.OnPropertyChanged(nameof(this.SelectedClient));
            }
        }

        /// <summary>
        /// The selected date.
        /// </summary>
        private DateTime selectedDate;

        /// <summary>
        /// Gets or sets the selected date.
        /// </summary>
        public DateTime SelectedDate
        {
            get
            {
                return this.selectedDate;
            }

            set
            {
                this.selectedDate = value;
                this.OnPropertyChanged(nameof(this.SelectedDate));
            }
        }

        /// <summary>
        /// The selected price.
        /// </summary>
        private string selectedPrice;

        /// <summary>
        /// Gets or sets the selected price.
        /// </summary>
        public string SelectedPrice
        {
            get
            {
                return this.selectedPrice;
            }

            set
            {
                this.selectedPrice = value;
                this.OnPropertyChanged(nameof(this.SelectedPrice));
            }
        }

        /// <summary>
        /// The view visibility.
        /// </summary>
        private Visibility viewVisibility = Visibility.Collapsed;

        /// <summary>
        /// Gets or sets the view visibility.
        /// </summary>
        public Visibility ViewVisibility
        {
            get
            {
                return this.viewVisibility;
            }

            set
            {
                this.viewVisibility = value;
                this.OnPropertyChanged(nameof(this.ViewVisibility));
            }
        }

        /// <summary>
        /// The cancel button visibility.
        /// </summary>
        private Visibility cancelVisibility;

        /// <summary>
        /// Gets or sets the cancel button visibility.
        /// </summary>
        public Visibility CancelVisibility
        {
            get
            {
                return this.cancelVisibility;
            }

            set
            {
                this.cancelVisibility = value;
                this.OnPropertyChanged(nameof(this.CancelVisibility));
            }
        }

        /// <summary>
        /// Gets the save command.
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                return new RelayCommand(this.Save);
            }
        }

        /// <summary>
        /// Gets the cancel command.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                return new RelayCommand(this.Cancel);
            }
        }

        /// <summary>
        /// The add client stub.
        /// </summary>
        private static readonly Client AddClientStub = new Client { Id = -1, FirstName = @"<Добавить клиента...>" };

        /// <summary>
        /// Gets or sets the clients.
        /// </summary>
        private static ObservableCollection<Client> Clients { get; set; }

        /// <summary>
        /// Gets or sets the payment.
        /// </summary>
        private Payment Payment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether new client is currently adding.
        /// </summary>
        private bool IsNewClientAdding { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentViewModel"/> class.
        /// </summary>
        public PaymentViewModel()
        {
            this.SelectedDate = DateTime.UtcNow;
            this.SelectedPrice = @"0";
            this.SelectedClient = null;

            this.PropertyChanged += (sender, property) =>
                {
                    if (property.PropertyName == nameof(this.SelectedClient))
                    {
                        this.HandleClientSelection();
                    }
                };

            PaymentsAggregator.PaymentSelected += (sender, payment) =>
                {
                    if (payment == null)
                    {
                        this.ViewVisibility = Visibility.Hidden;
                    }
                    else
                    {
                        this.LoadPayment(payment);
                        this.ViewVisibility = Visibility.Visible;
                    }
                };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentViewModel"/> class.
        /// </summary>
        /// <param name="payment">
        /// The payment.
        /// </param>
        public PaymentViewModel(Payment payment) : this()
        {
            this.LoadPayment(payment);
        }

        /// <summary>
        /// Loads the payment.
        /// </summary>
        /// <param name="payment">
        /// The payment.
        /// </param>
        private void LoadPayment(Payment payment)
        {
            this.Payment = payment;
            this.SelectedDate = this.Payment.Date != default(DateTime) ? this.Payment.Date : DateTime.UtcNow;
            this.SelectedPrice = this.Payment.Price.ToString(CultureInfo.InvariantCulture);
            this.SelectedClient = this.Payment.Client;

            this.CancelVisibility = this.Payment.Id == 0 ? Visibility.Visible : Visibility.Collapsed;
        }

        /// <summary>
        /// Handles client selection.
        /// </summary>
        private void HandleClientSelection()
        {
            if (this.SelectedClient == AddClientStub)
            {
                if (!this.IsNewClientAdding)
                {
                    this.IsNewClientAdding = true;

                    var newClientDialog = new CreateClientDialog();
                    if (newClientDialog.ShowDialog() == true)
                    {
                        var client = new Client { FirstName = newClientDialog.FirstName, LastName = newClientDialog.LastName };

                        new ClientsRepository().Create(client);
                        this.PaymentClients.Add(client);
                    }
                }
                else
                {
                    this.IsNewClientAdding = false;

                    this.SelectedClient = null; 
                }
            }

            if ((this.SelectedClient != null) && !this.PaymentClients.Contains(this.SelectedClient))
            {
                this.SelectedClient = this.PaymentClients.FirstOrDefault(client => client.Id == this.SelectedClient.Id);
            }
        }

        /// <summary>
        /// Tries to create a payment.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/> value indicating whether the payment was crested successfully.
        /// </returns>
        private bool TryCreatePayment()
        {
            int price;
            if (!int.TryParse(this.SelectedPrice, out price))
            {
                return false;
            }

            if ((this.SelectedClient == AddClientStub) || (this.SelectedClient == null))
            {
                return false;
            }

            if (this.Payment == null)
            {
                this.Payment = new Payment();
            }

            this.Payment.ClientId = this.SelectedClient.Id;
            this.Payment.Date = this.SelectedDate;
            this.Payment.Price = price;

            return true;
        }

        /// <summary>
        /// Handles the <code>Save</code> command.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        private void Save(object obj)
        {
            if (!this.TryCreatePayment())
            {
                return;
            }

            if (this.Payment.Id == 0)
            {
                new PaymentsRepository().Create(this.Payment);
                PaymentsAggregator.Add(this.Payment);
            }
            else
            {
                new PaymentsRepository().Update(this.Payment);
            }
            
            PaymentsAggregator.RecalculateTotalSumm();
        }
        
        /// <summary>
        /// Handles the <code>Cancel</code> command.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        private void Cancel(object obj)
        {
            PaymentsAggregator.Selected = null;
        }

        /// <summary>
        /// The <code>OnPropertyChanged</code> event handler.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
