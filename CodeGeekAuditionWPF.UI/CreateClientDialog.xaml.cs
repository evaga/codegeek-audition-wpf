﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="CreateClientDialog.xaml.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the CreateClientDialog.xaml type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.UI
{
    using System.Windows;

    /// <summary>
    /// The create client dialog.
    /// </summary>
    public partial class CreateClientDialog : Window
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateClientDialog"/> class.
        /// </summary>
        public CreateClientDialog()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// The <code>Create</code> button <code>OnClick</code> event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        private void CreateButtonOnClick(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(this.FirstName) || string.IsNullOrEmpty(this.FirstNameInput.Text))
            {
                this.FirstNameInput.Text = string.Empty;
                return;
            }

            if (string.IsNullOrEmpty(this.LastName) || string.IsNullOrEmpty(this.LastNameInput.Text))
            {
                this.LastNameInput.Text = string.Empty;
                return;
            }

            this.DialogResult = true;
        }
    }
}
