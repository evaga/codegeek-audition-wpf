﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="App.xaml.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the App.xaml type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.UI
{
    using System.Configuration;
    using System.Linq;
    using System.Windows;

    using CodeGeekAuditionWPF.DAL;

    /// <summary>
    /// The application.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            if (ConfigurationManager.ConnectionStrings.Count == 0)
            {
                throw new ConfigurationErrorsException(@"There is no connection string specified");
            }

            var connectionString = ConfigurationManager.ConnectionStrings.OfType<ConnectionStringSettings>().FirstOrDefault(str => str.Name == @"DefaultConnectionString");
            if (connectionString == null)
            {
                throw new ConfigurationErrorsException(@"There is no default connection string specified");
            }

            ConnectionProvider.Initialize(connectionString.ConnectionString);
        }
    }
}
