﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="PaymentsAggregator.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the PaymentsAggregator type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Linq;

    using CodeGeekAuditionWPF.DAL.Payments;

    /// <summary>
    /// The payments aggregator.
    /// </summary>
    public static class PaymentsAggregator
    {
        /// <summary>
        /// The payment selected event.
        /// </summary>
        public static event EventHandler<Payment> PaymentSelected;

        /// <summary>
        /// The total summ changed event.
        /// </summary>
        public static event EventHandler<decimal> TotalChanged;

        /// <summary>
        /// The selected payment.
        /// </summary>
        private static Payment selected;

        /// <summary>
        /// Gets or sets the selected payment.
        /// </summary>
        public static Payment Selected
        {
            get
            {
                return selected;
            }

            set
            {
                selected = value;
                OnPaymentSelected(selected);
            }
        }

        /// <summary>
        /// The filtered payments.
        /// </summary>
        private static ObservableCollection<Payment> filtered;

        /// <summary>
        /// Gets the filtered payments.
        /// </summary>
        public static ObservableCollection<Payment> Filtered
        {
            get
            {
                if (filtered == null)
                {
                    filtered = new ObservableCollection<Payment>(Payments);
                    RecalculateTotalSumm();
                }

                return filtered;
            }
        }

        /// <summary>
        /// The payments.
        /// </summary>
        private static ObservableCollection<Payment> payments;

        /// <summary>
        /// Gets the payments.
        /// </summary>
        private static ObservableCollection<Payment> Payments
        {
            get
            {
                if (payments == null)
                {
                    payments = new ObservableCollection<Payment>(new PaymentsRepository().Get());
                    payments.CollectionChanged += (sender, args) =>
                        {
                            if (args.Action == NotifyCollectionChangedAction.Add)
                            {
                                foreach (var payment in Filter(args.NewItems.OfType<Payment>()))
                                {
                                    Filtered.Add(payment);
                                }
                            }

                            if (args.Action == NotifyCollectionChangedAction.Remove)
                            {
                                foreach (var payment in args.OldItems.OfType<Payment>())
                                {
                                    Filtered.Remove(payment);
                                }
                            }

                            RecalculateTotalSumm();
                        };
                }

                return payments;
            }
        }

        /// <summary>
        /// The total summ.
        /// </summary>
        private static decimal TotalSumm { get; set; }

        /// <summary>
        /// The from date.
        /// </summary>
        private static DateTime fromDateFilter = DateTime.MinValue;

        /// <summary>
        /// The to date.
        /// </summary>
        private static DateTime toDateFilter = DateTime.MaxValue;

        /// <summary>
        /// The name.
        /// </summary>
        private static string nameFilter = string.Empty;

        /// <summary>
        /// Adds a payment
        /// </summary>
        /// <param name="payment">
        /// The payment.
        /// </param>
        public static void Add(Payment payment)
        {
            Payments.Add(payment);
        }

        /// <summary>
        /// Removes a payment.
        /// </summary>
        /// <param name="payment">
        /// The payment.
        /// </param>
        public static void Remove(Payment payment)
        {
            Payments.Remove(payment);
        }

        /// <summary>
        /// Applies the new filter.
        /// </summary>
        /// <param name="clientName">
        /// The client name.
        /// </param>
        /// <param name="from">
        /// The date from.
        /// </param>
        /// <param name="to">
        /// The date to.
        /// </param>
        public static void ApplyFilter(string clientName = null, DateTime? from = null, DateTime? to = null)
        {
            if (clientName != null)
            {
                nameFilter = clientName;
            }

            if (from != null)
            {
                fromDateFilter = from.Value;
            }

            if (to != null)
            {
                toDateFilter = to.Value;
            }

            Filtered.Clear();
            foreach (var payment in Filter(Payments))
            {
                Filtered.Add(payment);
            }

            RecalculateTotalSumm();
        }

        /// <summary>
        /// Recalculates the total summ.
        /// </summary>
        public static void RecalculateTotalSumm()
        {
            TotalSumm = Filtered.Sum(payment => payment.Price);
            OnTotalChanged(TotalSumm);
        }

        /// <summary>
        /// Filters the payments collection.
        /// </summary>
        /// <param name="paymentsToFilter">
        /// The payments.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/> with filtered collection.
        /// </returns>
        private static IEnumerable<Payment> Filter(IEnumerable<Payment> paymentsToFilter)
        {
            foreach (var payment in paymentsToFilter)
            {
                if (payment == null)
                {
                    continue;
                }

                if (payment.Date < fromDateFilter)
                {
                    continue;
                }

                if (payment.Date > toDateFilter)
                {
                    continue;
                }

                if (payment.Client.ToString().IndexOf(nameFilter, StringComparison.InvariantCultureIgnoreCase) < 0)
                {
                    continue;
                }

                yield return payment;
            }
        }

        /// <summary>
        /// The on payment selected event.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        private static void OnPaymentSelected(Payment e)
        {
            PaymentSelected?.Invoke(null, e);
        }

        /// <summary>
        /// The on total changed event.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        private static void OnTotalChanged(decimal e)
        {
            TotalChanged?.Invoke(null, e);
        }
    }
}
