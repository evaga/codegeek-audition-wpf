﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="ClientValidationRule.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the ClientValidationRule type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------
namespace CodeGeekAuditionWPF.UI.ValidationRules
{
    using System.Globalization;
    using System.Windows.Controls;

    using CodeGeekAuditionWPF.DAL.Clients;

    /// <summary>
    /// The client validation rule.
    /// </summary>
    public class ClientValidationRule : ValidationRule
    {
        /// <summary>
        /// Performs validation checks on a value.
        /// </summary>
        /// <param name="value">
        /// The value from the binding target to check.
        /// </param>
        /// <param name="cultureInfo">
        /// The culture to use in this rule.
        /// </param>
        /// <returns>
        /// The <see cref="ValidationResult"/> object.
        /// </returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            return new ValidationResult((value as Client) != null, "Client is not selected");
        }
    }
}
