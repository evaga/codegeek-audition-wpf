﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="NumberValidationRule.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the NumberValidationRule type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.UI.ValidationRules
{
    using System.Globalization;
    using System.Windows.Controls;

    /// <summary>
    /// The number validation rule.
    /// </summary>
    public class NumberValidationRule : ValidationRule
    {
        /// <summary>
        /// Performs validation checks on a value.
        /// </summary>
        /// <param name="value">
        /// The value from the binding target to check.
        /// </param>
        /// <param name="cultureInfo">
        /// The culture to use in this rule.
        /// </param>
        /// <returns>
        /// The <see cref="ValidationResult"/> object.
        /// </returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            double result;
            bool canConvert = double.TryParse(value as string, out result);
            return new ValidationResult(canConvert, "Not a valid double");
        }
    }
}
