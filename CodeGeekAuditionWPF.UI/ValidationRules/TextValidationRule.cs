﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="TextValidationRule.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the TextValidationRule type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.UI.ValidationRules
{
    using System.Globalization;
    using System.Windows.Controls;

    /// <summary>
    /// The text validation rule.
    /// </summary>
    public class TextValidationRule : ValidationRule
    {
        /// <summary>
        /// Performs validation checks on a value.
        /// </summary>
        /// <param name="value">
        /// The value from the binding target to check.
        /// </param>
        /// <param name="cultureInfo">
        /// The culture to use in this rule.
        /// </param>
        /// <returns>
        /// The <see cref="ValidationResult"/> object.
        /// </returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            return new ValidationResult(!string.IsNullOrEmpty(value as string), "Not a valid string");
        }
    }
}
