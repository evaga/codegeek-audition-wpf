﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="FiltersView.xaml.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the FiltersView.xaml type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.UI.Views
{
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// The filters view.
    /// </summary>
    public partial class FiltersView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FiltersView"/> class.
        /// </summary>
        public FiltersView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// The <code>Name</code> input <code>OnKeyDown</code> event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        private void NameInputOnKeyDown(object sender, KeyEventArgs e)
        {
            PaymentsAggregator.ApplyFilter(this.Name.Text);
        }

        /// <summary>
        /// The <code>FromDate</code> input <code>OnSelectedDateChanged</code> event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        private void FromDateInputOnSelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            PaymentsAggregator.ApplyFilter(null, this.FromDate.SelectedDate);
        }

        /// <summary>
        /// The <code>FromDate</code> input <code>OnSelectedDateChanged</code> event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        private void ToDateInputOnSelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            PaymentsAggregator.ApplyFilter(null, null, this.ToDate.SelectedDate);
        }
    }
}
