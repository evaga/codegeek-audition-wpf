﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="PaymentsTableView.xaml.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the PaymentsTableView.xaml type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.UI.Views
{
    using System.Windows.Controls;

    /// <summary>
    /// The payments table view.
    /// </summary>
    public partial class PaymentsTableView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentsTableView"/> class.
        /// </summary>
        public PaymentsTableView()
        {
            this.InitializeComponent();
        }
    }
}
