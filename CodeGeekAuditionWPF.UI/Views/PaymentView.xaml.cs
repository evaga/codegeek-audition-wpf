﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="PaymentView.xaml.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the PaymentView.xaml type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------
namespace CodeGeekAuditionWPF.UI.Views
{
    using System.Windows.Controls;

    /// <summary>
    /// The payment view.
    /// </summary>
    public partial class PaymentView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentView"/> class.
        /// </summary>
        public PaymentView()
        {
            this.InitializeComponent();
        }
    }
}
