﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="ControlsView.xaml.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the ControlsView.xaml type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.UI.Views
{
    using System.Windows;
    using System.Windows.Controls;

    using CodeGeekAuditionWPF.DAL.Payments;

    /// <summary>
    /// The controls view.
    /// </summary>
    public partial class ControlsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlsView"/> class.
        /// </summary>
        public ControlsView()
        {
            this.InitializeComponent();
            PaymentsAggregator.PaymentSelected += (sender, payment) => { this.RemoveButton.IsEnabled = (payment != null) && (payment.Id != 0); };
            PaymentsAggregator.TotalChanged += (sender, summ) => { this.Summ.Content = summ; };
        }

        /// <summary>
        /// The <code>Create</code> button <code>OnClick</code> event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        private void CreateButtonOnClick(object sender, RoutedEventArgs e)
        {
            PaymentsAggregator.Selected = new Payment();
        }
        
        /// <summary>
        /// The <code>Remove</code> button <code>OnClick</code> event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        private void RemoveButtonOnClick(object sender, RoutedEventArgs e)
        {
            if (PaymentsAggregator.Selected != null)
            {
                new PaymentsRepository().Remove(PaymentsAggregator.Selected);
                PaymentsAggregator.Remove(PaymentsAggregator.Selected);
            }
        }
    }
}
