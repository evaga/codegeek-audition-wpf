﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="ConnectionProvider.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the ConnectionProvider.cs type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.DAL
{
    using System;
    using System.Data.SqlClient;

    using CodeGeekAuditionWPF.DAL.Clients;
    using CodeGeekAuditionWPF.DAL.Payments;

    using SQLite;

    /// <summary>
    /// The connection provider.
    /// </summary>
    public static class ConnectionProvider
    {
        /// <summary>
        /// Gets or sets the instance.
        /// </summary>
        private static SQLiteConnection Instance { get; set; }

        /// <summary>
        /// Gets the connection.
        /// </summary>
        /// <returns>
        /// The <see cref="SQLiteConnection"/> connection.
        /// </returns>
        public static SQLiteConnection GetConnection()
        {
            if (Instance == null)
            {
                throw new NullReferenceException(@"Connection is not initiated");
            }

            return Instance;
        }

        /// <summary>
        /// Initializes connection.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        public static void Initialize(string connectionString)
        {
            var connectionStringBuilder = new SqlConnectionStringBuilder(connectionString);
            var path = connectionStringBuilder.DataSource;

            Instance = new SQLiteConnection(path);
            Instance.CreateTable<Client>();
            Instance.CreateTable<Payment>();
        }
    }
}
