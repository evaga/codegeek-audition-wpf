﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="BaseEntity.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the BaseEntity.cs type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.DAL.Base
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using CodeGeekAuditionWPF.DAL.Annotations;

    using SQLite;
    //using SQLite.Net.Attributes;

    /// <summary>
    /// The base entity.
    /// </summary>
    public abstract class BaseEntity: INotifyPropertyChanged
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        /// <summary>
        /// The property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Sets the field with value and notifies the change.
        /// </summary>
        /// <typeparam name="T">
        /// The type of field.
        /// </typeparam>
        /// <param name="field">
        /// The field.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        protected void SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            field = value;
            this.OnPropertyChanged(propertyName);
        }

        /// <summary>
        /// The <code>OnPropertyChanged</code> event handler.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
