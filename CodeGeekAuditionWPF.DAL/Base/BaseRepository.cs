﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="BaseRepository.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the BaseRepository.cs type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.DAL.Base
{
    using System.Collections.Generic;
    using System.Linq;

    using SQLite;
    //using SQLite.Net;

    /// <summary>
    /// The base repository.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the repository.
    /// </typeparam>
    public class BaseRepository<T> where T : BaseEntity, new()
    {
        /// <summary>
        /// Gets or sets the connection.
        /// </summary>
        private SQLiteConnection Connection { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepository{T}"/> class.
        /// </summary>
        public BaseRepository()
        {
            this.Connection = ConnectionProvider.GetConnection();
        }

        /// <summary>
        /// Gets all records of current type.
        /// </summary>
        /// <returns>
        /// The <see cref="IList{T}"/> of records.
        /// </returns>
        public IList<T> Get()
        {
            return this.Connection.Table<T>().ToList();
        }

        /// <summary>
        /// Gets the record of current type by Id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="T"/> record.
        /// </returns>
        public T Get(int id)
        {
            if (id == 0)
            {
                return null;
            }

            return this.Connection.Get<T>(record => record.Id == id);
        }

        /// <summary>
        /// Creates a record of current type.
        /// </summary>
        /// <param name="record">
        /// The record to create.
        /// </param>
        public void Create(T record)
        {
            this.Connection.Insert(record);
        }

        /// <summary>
        /// Updates a record of current type.
        /// </summary>
        /// <param name="record">
        /// The record to update.
        /// </param>
        public void Update(T record)
        {
            this.Connection.Update(record);
        }

        /// <summary>
        /// Removes a record of current type.
        /// </summary>
        /// <param name="record">
        /// The record to remove.
        /// </param>
        public void Remove(T record)
        {
            this.Connection.Delete(record);
        }
    }
}
