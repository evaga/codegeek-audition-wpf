﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="ClientsRepository.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the ClientsRepository.cs type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.DAL.Clients
{
    using CodeGeekAuditionWPF.DAL.Base;

    /// <summary>
    /// The clients repository.
    /// </summary>
    public class ClientsRepository : BaseRepository<Client>
    {
    }
}
