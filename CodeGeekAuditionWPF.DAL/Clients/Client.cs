﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="Client.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the Client.cs type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------
namespace CodeGeekAuditionWPF.DAL.Clients
{
    using CodeGeekAuditionWPF.DAL.Base;

    using SQLite;

    /// <summary>
    /// The client.
    /// </summary>
    public class Client : BaseEntity
    {
        /// <summary>
        /// The first name.
        /// </summary>
        private string firstName;

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [MaxLength(255)]
        public string FirstName
        {
            get { return this.firstName; }
            set { this.SetField(ref this.firstName, value); }
        }

        /// <summary>
        /// The last name.
        /// </summary>
        private string lastName;

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [MaxLength(255)]
        public string LastName
        {
            get { return this.lastName; }
            set { this.SetField(ref this.lastName, value); }
        }

        /// <summary>
        /// Gets a string representing the client.
        /// </summary>
        /// <returns>
        /// The string representing the client.
        /// </returns>
        public override string ToString()
        {
            if (string.IsNullOrEmpty(this.FirstName))
            {
                return this.LastName;
            }

            if (string.IsNullOrEmpty(this.LastName))
            {
                return this.FirstName;
            }

            return $"{this.FirstName} {this.LastName}";
        }
    }
}
