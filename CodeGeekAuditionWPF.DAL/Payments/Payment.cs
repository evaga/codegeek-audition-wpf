﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="Payment.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the Payment.cs type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------
namespace CodeGeekAuditionWPF.DAL.Payments
{
    using System;

    using CodeGeekAuditionWPF.DAL.Base;
    using CodeGeekAuditionWPF.DAL.Clients;

    using SQLite;

    /// <summary>
    /// The payment.
    /// </summary>
    public class Payment : BaseEntity
    {
        /// <summary>
        /// The client id.
        /// </summary>
        private int clientId;

        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        [Indexed]
        public int ClientId
        {
            get { return this.clientId; }
            set
            {
                this.SetField(ref this.clientId, value);
                this.SetClient(this.ClientId);
            }
        }

        /// <summary>
        /// The date.
        /// </summary>
        private DateTime date;

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public DateTime Date
        {
            get { return this.date; }
            set { this.SetField(ref this.date, value); }
        }

        /// <summary>
        /// The price.
        /// </summary>
        private decimal price;

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        public decimal Price
        {
            get { return this.price; }
            set { this.SetField(ref this.price, value); }
        }

        /// <summary>
        /// The client.
        /// </summary>
        private Client client;

        /// <summary>
        /// Gets the client.
        /// </summary>
        [Ignore]
        public Client Client
        {
            get
            {
                if (this.client == null)
                {
                    this.SetClient(this.ClientId);
                }

                return this.client;
            }
        }

        /// <summary>
        /// Sets a client.
        /// </summary>
        /// <param name="id">
        /// The id of client.
        /// </param>
        private void SetClient(int id)
        {
            this.client = new ClientsRepository().Get(id);

            if (this.client != null)
            {
                this.client.PropertyChanged += (sender, args) =>
                {
                    this.OnPropertyChanged(nameof(this.Client));
                };
            }

            this.OnPropertyChanged(nameof(this.Client));
        }
    }
}
