﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="PaymentsRepository.cs" company="evagapov">
//    Evgeny Agapov, 2016, CodeGeek audition test project.
//  </copyright>
//  <summary>
//    Defines the PaymentsRepository.cs type.
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace CodeGeekAuditionWPF.DAL.Payments
{
    using CodeGeekAuditionWPF.DAL.Base;

    /// <summary>
    /// The payments repository.
    /// </summary>
    public class PaymentsRepository: BaseRepository<Payment>
    {
    }
}
